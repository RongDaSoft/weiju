## 汇溪和他们的小伙伴们

> **汇溪**： 汇溪成海之意，象征汇聚多方力量。



感谢大家的支持😉。没想到有这么多的人愿意加入，真是受宠若惊呀😆。很多的东西都没有规划好，毕竟第一次（多多见谅）。

经验尚浅，许多东西不懂，劳烦各位了😜



## 项目配置

1. 微信小程序的配置

   ```yaml
   在 application.yml 文件中（在IDEA 中双击 shift搜索）
   
   weiJu:
     appId:
     appSecret:
     
     填上你申请小程序的 appid 和 appSecret 
   ```

2. 数据库 （mysql）

   ```
   数据库ip: 47.106.225.52
   用户名： root
   密码： weiju
   
   搭建的一个测试库随便搞
   ```

3. Redis

   ``` yaml
   host: 47.106.225.52
   port: 6379
   password: weiju
   ```

4.  **SpringSecurity + swagger **   :star:  :star: 

   由于项目采用的SpringSecurity 用了token作为凭证，先登录才行获取token才行 

   > https://www.weiju.fun/weiju/login  先从这个地址登录，用户名：weiju   密码：随便填
   >
   > https://www.weiju.fun/weiju/swagger-ui.html  调试swagger必须点击 右上角的 **Authorize**，填写token值



## 补充事项

1.记得在IDE上下载阿里巴巴的编码检测	Alibaba Java Coding Guidelines，**力求规范**

2.小程序的Git地址：https://gitee.com/huixi_and_their_friends/weiju-wechat.git

3.小程序后台的Git地址：https://gitee.com/huixi_and_their_friends/weiju.git



## 项目结构简介

```
microspur
├── microspur-commons -- 通用的公共模块
├	└── com.huixi.microspur.commons
├		├── base.BaseContrller -- 所有模块都的controller都继承了它
├		├── config -- 公共配置
├		├── constant -- 公共常量
├		├── enums -- 公共错误码
├		├── exception -- 异常拦截统一包装
├		└── util （工具类用 hutool）
├			├── id -- 统一id生成器
├			├── validators -- 统一验证器
├			└── wrapper -- controller返回值包装类
├── microspur-generator -- 代码生成
├── microspur-web -- 小程序
├	└── com.huixi.microspur.web
├    	├── aspect -- 异常拦截器
├    	├── config -- 配置文件
├    	├── controller -- 接口
├    	├── entity -- 实体类
├    	├── mapper -- 方法接口
├  		└── service -- 方法实现
└── microspur-sysadmin -- 管理后台（结构同上）

项目分两个包部署。 一个应对小程序 另一个应对管理后台
```



## 项目注意事项（有什么好规范可写在下方）

3. 文件存储

   > 文件的存储用的是阿里的OSS 。 一系列配置都在文件中有。 我分派的是一个子账号，权限只能操作OSS，不用担心安全问题。

4. 使用mybatis-plus 是一律使用 **IService**, 没有必要去过度封装。有的小伙伴是第一次使用mybatis-plus，可以减轻阅读代码的压力，也可参照官方文档。

   > [https://mp.baomidou.com/guide/crud-interface.html#service-crud-%E6%8E%A5%E5%8F%A3](https://mp.baomidou.com/guide/crud-interface.html#service-crud-接口)
   
5. 在执行 git 的 push  操作时，注释写得详细一些。  另外 你可以在本地建一条分支，但是不提交到码云上。与本地的dev 分支合并后，提交 dev 分支。

6. 自己写过的代码，必须有Javadoc 注释（嫌写着麻烦的同学，推荐IDEA一款插件easy-javadoc，可以一键生成注释）。可参照下面模板；最重要的是 **作用，作者，时间**

   ```java
   	/**
        * 获取用户加密信息 并且保存
        * @Author 叶秋
        * @Date 2020/2/5 13:44
        **/
   ```

7. 需要改动数据库表字段等操作。需要到群中通知一声

8. 一些小功能，比如判断空值，UUID，排序... 的一些操作，可用 **hutool**  更加的简洁

   > https://www.hutool.cn/

9. 



