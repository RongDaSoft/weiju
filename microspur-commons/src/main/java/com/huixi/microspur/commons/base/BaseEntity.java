package com.huixi.microspur.commons.base;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.huixi.microspur.commons.constant.Constant;
import com.huixi.microspur.commons.enums.DelFlagEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 基础实体类，所有实体都需要继承
 */
@Data
public abstract class BaseEntity implements Serializable {



    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    public LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     *  逻辑删除
     */
    @ApiModelProperty(value = "逻辑删除标志(做删除操作是会作为修改方法 ) N 不删除， Y 已经删除")
    @TableLogic(value = Constant.NORMAL, delval = Constant.DEL)
    @TableField(value = "del_flag",fill = FieldFill.INSERT)
    private String delFlag;

    /**
     *  是否启用
     */
    @ApiModelProperty(value = "是否启用 Y 启用， N 禁用")
    @TableField(value = "enable_flag",fill = FieldFill.INSERT_UPDATE)
    private String enableFlag;



}
