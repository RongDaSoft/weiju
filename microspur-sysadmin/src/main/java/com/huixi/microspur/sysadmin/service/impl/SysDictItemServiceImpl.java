package com.huixi.microspur.sysadmin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.sysadmin.entity.SysDictItem;
import com.huixi.microspur.sysadmin.mapper.SysDictItemMapper;
import com.huixi.microspur.sysadmin.service.SysDictItemService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典子表 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
@Service
public class SysDictItemServiceImpl extends ServiceImpl<SysDictItemMapper, SysDictItem> implements SysDictItemService {

}
