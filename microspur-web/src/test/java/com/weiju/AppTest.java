package com.weiju;


import com.huixi.microspur.commons.enums.UserTagEnum;
import com.huixi.microspur.web.WebApplication;
import com.huixi.microspur.web.service.*;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Unit test for simple App.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AppTest {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    WjAppealService wjAppealService;

    @Autowired
    WjDynamicService wjDynamicService;

    @Autowired
    WjChatService wjChatService;

    @Autowired
    WjChatUserService wjChatUserService;


    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {

//        stringRedisTemplate.opsForValue().set("hello","world");

    }


    @Test
    public void test1() {

//        QueryWrapper<com.huixi.microspur.web.pojo.entity.Test> queryWrapper = new QueryWrapper<>();
//        queryWrapper.orderByDesc("id");
//
//        IPage<com.huixi.microspur.web.pojo.entity.Test> testIPage = new Page<>(1, 10);//参数一是当前页，参数二是每页个数
//        testIPage = testService.page(testIPage, queryWrapper);
//
//        List<com.huixi.microspur.web.pojo.entity.Test> records = testIPage.getRecords();
//        for (com.huixi.microspur.web.pojo.entity.Test record : records) {
//            System.out.println(record);
//        }


    }

    /**
     * 添加 动态
     *
     * @param
     * @return void
     * @Author 叶秋
     * @Date 2020/4/19 21:32
     **/
    @Test
    public void test2() {

//        WjDynamic wjDynamic = new WjDynamic();
//
//        wjDynamic.setDynamicId(IdUtil.simpleUUID());
//        wjDynamic.setUserId("76d363f5df774f5b965aae33dbb4e536");
//        wjDynamic.setContent("这是一条测试的动态，谢谢大家的支持。我会好好加油干的");
//        wjDynamic.setUrl("https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/channels4_banner.jpg");
//        wjDynamic.setFileType("JPG");
//
//        boolean save = wjDynamicService.save(wjDynamic);


    }


    /**
     * 添加 聊天室
     *
     * @param
     * @return void
     * @Author 叶秋
     * @Date 2020/4/26 23:26
     **/
    @Test
    public void test3() {

//        WjChat wjChat = new WjChat();
//        wjChat.setChatId(IdUtil.simpleUUID());
//        wjChat.setType("one");
//        wjChat.setTitle("Hello World2");
//        wjChat.setPeopleNumber(1);
//
//        wjChatService.save(wjChat);


    }


    /**
     *  添加聊天室的用户
     * @Author 叶秋
     * @Date 2020/4/26 23:48
     * @param
     * @return void
     **/
    @Test
    public void test4(){

//        WjChatUser wjChatUser = new WjChatUser();
//
//        wjChatUser.setChatId("a8ae3ad7bd2a420f82f4c19d3832adf1");
//        wjChatUser.setUserId("c6837ade5a894e7f8d2fac512ded52dd");
//
//        wjChatUserService.save(wjChatUser);

    }


    @Test
    public void test5(){

//        String fileName = "1.jpg";
//
//        String substring = fileName.substring(fileName.lastIndexOf("."));
//
//        System.out.println(substring);




    }


}
