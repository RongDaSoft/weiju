package com.huixi.microspur.web.controller.appeal;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.enums.ErrorCodeEnum;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealEndorse;
import com.huixi.microspur.web.service.WjAppealEndorseService;
import com.huixi.microspur.web.util.CommonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 *  诉求点赞 模块
 * @Author 叶秋
 * @Date 2020/3/18 22:17
 * @param
 * @return
 **/
@RestController
@RequestMapping("/wjAppealEndorse")
@Api(tags = "诉求-点赞的接口")
public class WjAppealEndorseController extends BaseController {


    @Resource
    private WjAppealEndorseService wjAppealEndorseService;

    /**
     *  为诉求点赞
     * @Author 叶秋
     * @Date 2020/3/18 22:24
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/appealToEndorse/{appealId}")
    @ApiOperation(value = "为诉求点赞", notes = "")
    public Wrapper appealToEndorse(@Valid @NotNull(message = "诉求id为空") @PathVariable String appealId){

        String nowUserId = CommonUtil.getNowUserId();

        WjAppealEndorse wjAppealEndorse = new WjAppealEndorse();
        wjAppealEndorse.setAppealId(appealId).setUserId(nowUserId);

        boolean save = wjAppealEndorseService.save(wjAppealEndorse);

        if(save){
            return WrapMapper.ok(true);
        }

        return WrapMapper.error(ErrorCodeEnum.APPEALENDORSE001);


    }

    /**
     *  取消帖子的点赞
     * @Author 叶秋
     * @Date 2020/3/20 0:29
     * @param appealId 诉求id
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @DeleteMapping("/cancelEndorse/{appealId}")
    @ApiOperation(value = "取消帖子的点赞", notes = "")
    public Wrapper cancelEndorse(@Valid @NotNull(message = "诉求id为空") @PathVariable String appealId){

        String nowUserId = CommonUtil.getNowUserId();

        QueryWrapper<WjAppealEndorse> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("appeal_id", appealId).
                eq("user_id", nowUserId);

        boolean remove = wjAppealEndorseService.remove(objectQueryWrapper);

        if(!remove){
            return WrapMapper.error(ErrorCodeEnum.APPEALCANCELENDORSE002);
        }

        return WrapMapper.ok(true);
    }





}
