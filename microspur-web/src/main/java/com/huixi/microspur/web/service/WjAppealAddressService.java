package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealAddress;

/**
 *  诉求地址 提供的服务类
 * @Author 叶秋 
 * @Date 2020/3/25 2:04
 * @param 
 * @return 
 **/
public interface WjAppealAddressService extends IService<WjAppealAddress> {
}
