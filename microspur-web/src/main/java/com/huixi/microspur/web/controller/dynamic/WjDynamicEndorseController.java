package com.huixi.microspur.web.controller.dynamic;

import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.pojo.entity.dynamic.WjDynamicEndorse;
import com.huixi.microspur.web.service.WjDynamicEndorseService;
import com.huixi.microspur.web.util.CommonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 动态点赞模块
 * @Author 叶秋
 * @Date 2020/6/22 23:57
 * @param
 * @return
 **/
@Slf4j
@RestController
@RequestMapping("/wjDynamicEndorse")
@Api(tags = "动态点赞-模块")
public class WjDynamicEndorseController extends BaseController {


    @Resource
    private WjDynamicEndorseService wjDynamicEndorseService;



    /**
     *  添加动态点赞 记录
     * @Author 叶秋
     * @Date 2020/4/20 22:11
     * @param
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @ApiOperation(value = "动态点赞")
    @PostMapping(value = "/dynamicEndorse/{dynamicId}")
    public Wrapper addDynamicEndorse(@Valid @NotNull(message = "动态id为空") @PathVariable String dynamicId){

        String nowUserId = CommonUtil.getNowUserId();

        WjDynamicEndorse wjDynamicEndorse = new WjDynamicEndorse().setDynamicId(dynamicId)
                .setUserId(nowUserId);


        boolean save = wjDynamicEndorseService.save(wjDynamicEndorse);


        return WrapMapper.ok(save);
    }


    /**
     *  取消动态的点赞
     * @Author 叶秋
     * @Date 2020/3/20 0:29
     * @param
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @ApiOperation(value = "取消动态的点赞")
    @DeleteMapping("/cancelEndorse/{dynamicId}")
    public Wrapper cancelEndorse(@Valid @NotNull(message = "动态id为空") @PathVariable String dynamicId){

        String nowUserId = CommonUtil.getNowUserId();

        WjDynamicEndorse wjDynamicEndorse = new WjDynamicEndorse().setDynamicId(dynamicId)
                .setUserId(nowUserId);


        Boolean aBoolean = wjDynamicEndorseService.cancelEndorse(wjDynamicEndorse);

        return WrapMapper.ok(aBoolean);
    }



}
