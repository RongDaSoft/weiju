package com.huixi.microspur.web.pojo.dto.appeal;


import com.huixi.microspur.commons.page.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 *  分页查询我的所有评论
 * @Author 叶秋
 * @Date 2020/6/12 0:05
 * @param
 * @return
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "分页查询我的评论", description = "分页查询我的评论")
public class MyAppealCommentPageDTO {

    @NotNull(message = "用户id为空")
    @ApiModelProperty(value = "用户id", required = true)
    private String userId;

    private PageQuery pageQuery;

}
