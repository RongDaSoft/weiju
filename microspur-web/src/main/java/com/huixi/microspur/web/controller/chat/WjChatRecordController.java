package com.huixi.microspur.web.controller.chat;


import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.pojo.vo.chat.WjChatRecordPageVO;
import com.huixi.microspur.web.service.WjChatRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 聊天室-聊天记录 前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Slf4j
@RestController
@RequestMapping("/wjChatRecord")
@Api(tags = "聊天室-聊天记录")
public class WjChatRecordController extends BaseController {


    @Resource
    private WjChatRecordService wjChatRecordService;


    /**
     *  分页查询参数
     * @Author 叶秋
     * @Date 2020/6/22 23:24
     * @param wjChatRecordPageVO
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @ApiOperation(value = "分页查询聊天记录")
    @PostMapping("/pageChatRecord")
    public Wrapper queryPageWjChatRecord(@Valid @RequestBody WjChatRecordPageVO wjChatRecordPageVO){


        PageData pageData = wjChatRecordService.queryPageWjChatRecord(wjChatRecordPageVO);


        return WrapMapper.ok(pageData);
    }

}

