package com.huixi.microspur.web.pojo.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 专门用来存储微信后台发送给我们的数据
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_user_wx")
@ApiModel(value="WjUserWx对象", description="专门用来存储微信后台发送给我们的数据")
public class WjUserWx implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户id")
    @TableId(value = "user_id", type = IdType.INPUT)
    private String userId;

    @ApiModelProperty(value = "用户的唯一标识")
    @TableField("wx_open_id")
    private String wxOpenId;

    @ApiModelProperty(value = "（冗余）")
    @TableField("wx_union_id")
    private String wxUnionId;

    @ApiModelProperty(value = "小程序的后台")
    @TableField("wx_app_id")
    private String wxAppId;


}
