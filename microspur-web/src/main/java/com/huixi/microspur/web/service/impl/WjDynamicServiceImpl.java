package com.huixi.microspur.web.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.commons.page.PageFactory;
import com.huixi.microspur.web.pojo.vo.dynamic.QueryDynamicVO;
import com.huixi.microspur.web.pojo.vo.dynamic.WjDynamicPageVO;
import com.huixi.microspur.web.pojo.entity.dynamic.WjDynamic;
import com.huixi.microspur.web.pojo.entity.user.WjUser;
import com.huixi.microspur.web.mapper.WjDynamicMapper;
import com.huixi.microspur.web.service.WjDynamicEndorseService;
import com.huixi.microspur.web.service.WjDynamicService;
import com.huixi.microspur.web.service.WjUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 动态表 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjDynamicServiceImpl extends ServiceImpl<WjDynamicMapper, WjDynamic> implements WjDynamicService {

    @Resource
    private WjUserService wjUserService;

    @Resource
    private WjDynamicEndorseService wjDynamicEndorseService;


    /**
     * 分页查询动态
     *
     * @param wjDynamicPageVO
     * @return java.util.List<com.huixi.microspur.web.pojo.vo.dynamic.QueryDynamicVO>
     * @Author 叶秋
     * @Date 2020/4/22 11:38
     **/
    @Override
    public PageData listPageDynamic(WjDynamicPageVO wjDynamicPageVO) {

        PageData pageData = new PageData<>();

        List<QueryDynamicVO> queryDynamicVOList = new ArrayList<>();


        // 查询条件
        QueryWrapper<WjDynamic> objectQueryWrapper = new QueryWrapper<>();
        if(wjDynamicPageVO.getCreateTime()){
            objectQueryWrapper.orderByDesc("create_time");
        }
        if(wjDynamicPageVO.getEndorseCount()){
            objectQueryWrapper.orderByDesc("endorse_count");
        }
        // 分页条件
        Page<WjDynamic> page = PageFactory.createPage(wjDynamicPageVO.getPageQuery());
        Page<WjDynamic> wjDynamicPage = page(page, objectQueryWrapper);


        QueryDynamicVO queryDynamicVO;
        // 循环判断查询
        for (WjDynamic record : wjDynamicPage.getRecords()) {

            queryDynamicVO = new QueryDynamicVO();

            BeanUtil.copyProperties(record, queryDynamicVO);

            // 判断是否点赞
            queryDynamicVO.setIsEndorse(false);
            if(StrUtil.isNotEmpty(wjDynamicPageVO.getUserId())) {
                Boolean endorse = wjDynamicEndorseService.isEndorse(record.getDynamicId(), wjDynamicPageVO.getUserId());
                queryDynamicVO.setIsEndorse(endorse);
            }

            // 点赞总数量
            int totalCount = wjDynamicEndorseService.getTotleCount(record.getDynamicId());
            queryDynamicVO.setEndorseCount(totalCount);

            // 发帖用户信息
            WjUser wjUser = wjUserService.getById(record.getUserId());
            queryDynamicVO.setNickName(wjUser.getNickName()).setHeadPortrait(wjUser.getHeadPortrait());


            queryDynamicVOList.add(queryDynamicVO);

        }

        BeanUtil.copyProperties(wjDynamicPage, pageData);
        pageData.setRecords(queryDynamicVOList);


        return pageData;
    }



}
