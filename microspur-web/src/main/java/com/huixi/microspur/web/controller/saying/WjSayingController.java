package com.huixi.microspur.web.controller.saying;

import com.huixi.microspur.web.pojo.entity.saying.*;
import com.huixi.microspur.web.service.WjSayingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import com.huixi.microspur.commons.page.PageFactory;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 语录表 前端控制器
 * </p>
 *
 * @author 碧海青天夜夜心
 * @since 2020-03-09
 */
@Slf4j
@Api(tags = "语录模块")
@RestController
@RequestMapping("/wj-sayings")
public class WjSayingController {

    @Resource
    private WjSayingService wjSayingService;

    /**
     * 描述：创建语录表信息
     *
     * @param wjSaying 请求体
     */
    @ApiOperation(value = "创建语录表信息", notes = "根据传来的参数创建语录表信息")
    @PostMapping
    public Wrapper save(@RequestBody WjSaying wjSaying) {
        wjSayingService.save(wjSaying);
        return WrapMapper.ok();
    }

    /**
     * 描述：（批量）删除语录表信息
     * @param ids 语录表id集合
     */
    @ApiOperation(value = "（批量）删除语录表信息", notes = "根据url的ids来（批量）删除语录表信息")
    @DeleteMapping(value = "/{ids}")
    public Wrapper removeByIds(@PathVariable("ids") List<String> ids) {
        wjSayingService.removeByIds(ids);
        return WrapMapper.ok();
    }

    /**
     * 描述：更新语录表信息
     * @param wjSaying 请求体
     */
    @ApiOperation(value = "更新语录表信息", notes = "根据传过来参数更新语录表信息")
    @PutMapping
    public Wrapper updateById(@RequestBody WjSaying wjSaying) {
        wjSayingService.updateById(wjSaying);
        return WrapMapper.ok();
    }

    /**
     * 描述：根据Id查询语录表详细信息
     *
     * @param id  ID
     */
    @ApiOperation(value = "获取语录表详细信息", notes = "根据url的id来获取语录表详细信息")
    @GetMapping(value = "/{id}")
    public Wrapper<WjSaying> getById(@PathVariable("id") String id) {
        return WrapMapper.ok(wjSayingService.getById(id));
    }

    /**
     * 描述：查询语录表列表信息
     */
    @ApiOperation(value = "查询语录表列表信息", notes = "根据传过来参数查询语录表列表信息")
    @GetMapping
    public Wrapper<List<WjSaying>> list() {
        return WrapMapper.ok(wjSayingService.list());
    }

    /**
     * 描述：根据参数查询语录表分页列表信息
     */
    @ApiOperation(value = "根据参数查询语录表分页列表信息", notes = "默认分页信息：从第1页，每页5条，参数名称：pageSize，pageNo，sort，orderBy")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageSize", value = "每页大小", defaultValue = "5", paramType = "path", dataTypeClass = Integer.class),
            @ApiImplicitParam(name = "pageNo", value = "第几页", defaultValue = "1", paramType = "path", dataTypeClass = Integer.class),
            @ApiImplicitParam(name = "sort", value = "升序还是降序", defaultValue = "asc", paramType = "path", dataTypeClass = String.class),
            @ApiImplicitParam(name = "orderBy", value = "字段排序", paramType = "path", dataTypeClass = String.class)
    })
    @GetMapping(value = "pages")
    public Wrapper<IPage<WjSaying>> page() {
        return WrapMapper.ok(wjSayingService.page(PageFactory.defaultPage()));
    }
}
