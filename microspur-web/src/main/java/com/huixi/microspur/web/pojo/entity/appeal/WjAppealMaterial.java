package com.huixi.microspur.web.pojo.entity.appeal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.microspur.commons.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 诉求素材表-存储素材涉及的图片，或者大文件
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_appeal_material")
@ApiModel(value="WjAppealMaterial对象", description="诉求素材表-存储素材涉及的图片，或者大文件")
public class WjAppealMaterial extends BaseEntity{

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "诉求素材表的id")
    @TableId(value = "appeal_material_id", type = IdType.ASSIGN_UUID)
    private String appealMaterialId;

    @ApiModelProperty(value = "诉求id")
    @TableField("appeal_id")
    private String appealId;

    @ApiModelProperty(value = "素材的url地址（存放在OSS）")
    @TableField("url")
    private String url;

    @ApiModelProperty(value = "素材格式(jpg,png.....)")
    @TableField("file_type")
    private String fileType;

    @ApiModelProperty(value = "素材大小(KB 为单位)")
    @TableField("file_size")
    private String fileSize;

    @ApiModelProperty(value = "素材的时间，未来可能可以上传视频(冗余)")
    @TableField("video_time")
    private String videoTime;

    @ApiModelProperty(value = "顺序，应对用户分批次上传(冗余)")
    @TableField("sequence")
    private Integer sequence;

    @ApiModelProperty(value = "创建人")
    @TableField("create_by")
    private String createBy;

    @ApiModelProperty(value = "修改人")
    @TableField("update_by")
    private String updateBy;



}
