package com.huixi.microspur.web.controller.dynamic;


import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.pojo.vo.dynamic.WjDynamicPageVO;
import com.huixi.microspur.web.service.WjDynamicService;
import com.huixi.microspur.web.util.CommonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 动态表 前端控制器
 * </p>
 *
 * @author 叶秋
 * @since 2020-01-17
 */
@Slf4j
@RestController
@RequestMapping("/wjDynamic")
@Api(tags = "动态模块")
public class WjDynamicController extends BaseController {


    @Resource
    private WjDynamicService wjDynamicService;



    /**
     *  分页查询动态
     * @Author 叶秋
     * @Date 2020/4/19 21:12
     * @param
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/pageDynamic")
    @ApiModelProperty(value = "分页查询动态")
    public Wrapper queryPageWjDynamic(@RequestBody WjDynamicPageVO wjDynamicPageVO){

        String nowUserId = CommonUtil.getNowUserId();
        wjDynamicPageVO.setUserId(nowUserId);

        PageData pageData = wjDynamicService.listPageDynamic(wjDynamicPageVO);


        return WrapMapper.ok(pageData);


    }



}

