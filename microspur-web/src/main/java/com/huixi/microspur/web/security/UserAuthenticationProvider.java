package com.huixi.microspur.web.security;

import com.huixi.microspur.web.pojo.entity.user.WjUser;
import com.huixi.microspur.web.service.WjUserService;
import com.huixi.microspur.web.service.WjUserWxService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;

/**
 * 自定义登录验证
 *
 * @Author Sans
 * @CreateTime 2019/10/1 19:11
 */
@Component
public class UserAuthenticationProvider implements AuthenticationProvider {

    @Resource
    private WjUserService wjUserService;

    @Resource
    private WjUserWxService wjUserWxService;

    @Resource
    private RedisTemplate redisTemplate;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // 获取表单输入中返回的用户名
        String code = (String) authentication.getPrincipal();
        // 获取表单中输入的密码
//        String password = (String) authentication.getCredentials();

        WjUser wjUser = null;

        if ("weiju".equals(code)) {

            wjUser = wjUserService.getById("f2ba6628b7204ba7af52fa1185ac0b60");

        } else {
            // 解析code
            wjUser = wjUserService.detectionUserAuthorization(code);
        }


        // 查询用户是否存在
//        SelfUserEntity userInfo = selfUserDetailsService.loadUserByUsername(userName);
//        if (userInfo == null) {
//            throw new UsernameNotFoundException("用户名不存在");
//        }
        // 我们还要判断密码是否正确，这里我们的密码使用BCryptPasswordEncoder进行加密的
//        if (!new BCryptPasswordEncoder().matches(password, userInfo.getPassword())) {
//            throw new BadCredentialsException("密码不正确");
//        }
        // 还可以加一些其他信息的判断，比如用户账号已停用等判断
//        if (userInfo.getStatus().equals("PROHIBIT")) {
//            throw new LockedException("该用户已被冻结");
//        }

        // 角色集合
        Set<GrantedAuthority> authorities = new HashSet<>();
        // 查询用户角色
//        List<SysRoleEntity> sysRoleEntityList = sysUserService.selectSysRoleByUserId(userInfo.getUserId());
//        for (SysRoleEntity sysRoleEntity : sysRoleEntityList) {
//            authorities.add(new SimpleGrantedAuthority("ROLE_" + sysRoleEntity.getRoleName()));
//        }
        wjUser.setAuthorities(authorities);
        // 进行登录
        return new UsernamePasswordAuthenticationToken(wjUser, authorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}