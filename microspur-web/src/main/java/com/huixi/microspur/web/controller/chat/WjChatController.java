package com.huixi.microspur.web.controller.chat;


import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.pojo.entity.chat.WjChat;
import com.huixi.microspur.web.service.WjChatService;
import com.huixi.microspur.web.util.CommonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * 聊天室（聊天列表） 前端控制器
 * </p>
 *
 * @author 叶秋
 * @since 2020-01-17
 */
@Slf4j
@RestController
@RequestMapping("/wjChat")
@Api(tags = "聊天室-模块")
public class WjChatController extends BaseController {


    @Resource
    private WjChatService wjChatService;


    /**
     *  根据用户id 查询用户所有的 聊天室/聊天列表（限制用户对话数，这样不用分页查询。）
     * @Author 叶秋 
     * @Date 2020/4/23 23:15
     * @param
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @ApiOperation(value = "查询用户所有的 聊天室/聊天列表（限制用户对话数，这样不用分页查询。）")
    @GetMapping("/chat")
    public Wrapper queryAllChat(){

        String nowUserId = CommonUtil.getNowUserId();

        List<WjChat> wjChatList = wjChatService.queryAllChat(nowUserId);


        return WrapMapper.ok(wjChatList);
        
    }





}

