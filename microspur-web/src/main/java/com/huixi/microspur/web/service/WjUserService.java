package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.web.pojo.entity.user.WjUser;
import com.huixi.microspur.web.pojo.dto.user.WxDecodeUserInfoDTO;
import com.huixi.microspur.web.pojo.dto.user.WxSesssionKeyDTO;
import com.huixi.microspur.web.pojo.vo.user.UserDataVO;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjUserService extends IService<WjUser> {


    /**
     * 根据code 获取session_key、open_id
     * @Author 李辉
     * @Date 2019/11/23 4:31
     * @param code 用户code
     * @return java.lang.String
     **/
    WxSesssionKeyDTO getWxSessionKey(String code);


    /**
     * 获取用户加密信息 后的信息
     * @Author 叶秋
     * @Date 2020/2/5 13:44
     * @param encryptedData
     * @param session_key
     * @param ivKey
     * @return WxDecodeUserInfoDTO
     **/
    WxDecodeUserInfoDTO getEncryptionUserInfo(String encryptedData, String session_key, String ivKey);



    /**
     *  根据用户code 获取用户的 openId 和 sessionKey , 来确定有没有授权过。 如果授权发放用户信息 ，没有就创建一个用户
     * @Author 叶秋
     * @Date 2020/2/1 20:17
     * @param code 用户小程序的code
     * @return WjUser 用户信息类
     **/
    WjUser detectionUserAuthorization(String code);


    

    /**
     *  根据用户id 查询用户信息 包括需要查询其他表的一些数据（点赞、动态...）
     * @Author 叶秋
     * @Date 2020/6/1 21:55
     * @param userId
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    UserDataVO getByIdUserData(String userId);



}
