package com.huixi.microspur.web.controller.appeal;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.commons.page.PageFactory;
import com.huixi.microspur.commons.page.PageQuery;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.pojo.dto.appeal.MyAppealCommentPageDTO;
import com.huixi.microspur.web.pojo.dto.appeal.WjAppealCommentDTO;
import com.huixi.microspur.web.pojo.dto.appeal.WjAppealCommentPageDTO;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealComment;
import com.huixi.microspur.web.service.WjAppealCommentService;
import com.huixi.microspur.web.util.CommonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 诉求-评论 前端控制器
 * </p>
 *
 * @author 叶秋
 * @since 2020-01-17
 */
@Slf4j
@RestController
@RequestMapping("/wjAppealComment")
@Api(tags = "诉求-评论模块")
public class WjAppealCommentController extends BaseController {


    @Resource
    private WjAppealCommentService wjAppealCommentService;


    /**
     *  添加诉求评论
     * @Author 叶秋
     * @Date 2020/4/13 22:23
     * @param appealCommentDTO
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/appealComment")
    @ApiOperation(value = "添加诉求评论")
    public Wrapper appealComment(@Valid @RequestBody WjAppealCommentDTO appealCommentDTO){

        String nowUserId = CommonUtil.getNowUserId();
        appealCommentDTO.setUserId(nowUserId);

        WjAppealComment wjAppealComment = new WjAppealComment();

        BeanUtil.copyProperties(appealCommentDTO, wjAppealComment);

        boolean save = wjAppealCommentService.save(wjAppealComment);

        return WrapMapper.ok(save);

    }



    /**
     *  删除 诉求评论
     * @Author 叶秋
     * @Date 2020/4/13 22:23
     * @param appealCommentId 诉求id
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @DeleteMapping("/appealComment/{appealCommentId}")
    @ApiOperation(value = "删除 诉求评论")
    @ApiImplicitParam(name = "appealCommentId", value = "诉求评论的id", dataType = "String"
    ,required = true)
    public Wrapper appealComment(@PathVariable String appealCommentId){

        boolean b = wjAppealCommentService.removeById(appealCommentId);

        return WrapMapper.ok(b);

    }


    /**
     *  分页查询我的诉求评论
     * @Author 叶秋
     * @Date 2020/6/18 22:38
     * @param myAppealCommentPageDTO
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/pageMyAppealComment")
    @ApiOperation(value = "分页查询我的诉求评论")
    public Wrapper pageMyAppealComment(@Valid @RequestBody MyAppealCommentPageDTO myAppealCommentPageDTO){

        PageData<Object> objectPageData = new PageData<>();

        Page<WjAppealComment> page = PageFactory.createPage(myAppealCommentPageDTO.getPageQuery());

        QueryWrapper<WjAppealComment> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("user_id", myAppealCommentPageDTO.getUserId());


        Page<WjAppealComment> wjAppealCommentPage = wjAppealCommentService.page(page, objectQueryWrapper);
        BeanUtil.copyProperties(wjAppealCommentPage, objectPageData);


        return WrapMapper.ok(objectPageData);

    }


    /**
     *  分页查询诉求的 评论
     * @Author 叶秋
     * @Date 2020/4/13 22:48
     * @param wjAppealCommentPageDTO
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/pageAppealComment")
    @ApiOperation(value = "分页查询诉求的 评论")
    public Wrapper queryPageAppealComment(@Valid @RequestBody WjAppealCommentPageDTO wjAppealCommentPageDTO) {

        PageData<Object> objectPageData = new PageData<>();

        Page<WjAppealComment> page = PageFactory.createPage(wjAppealCommentPageDTO.getPageQuery());

        QueryWrapper<WjAppealComment> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.orderByDesc("create_time").eq("appeal_id", wjAppealCommentPageDTO.getAppealId());


        Page<WjAppealComment> wjAppealCommentPage = wjAppealCommentService.page(page, objectQueryWrapper);

        BeanUtil.copyProperties(wjAppealCommentPage, objectPageData);


        return WrapMapper.ok(objectPageData);

    }


    /**
     *  分页查询我的诉求评论(还不知道前端需要啥参，就都给了)
     * @Author 叶秋
     * @Date 2020/7/2 23:11
     * @param pageQuery 分页参数
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/queryMyAppealComment")
    @ApiOperation(value = "分页查询诉求 我的 评论")
    public Wrapper queryMyAppealComment(@RequestBody PageQuery pageQuery){

        PageData<Object> objectPageData = new PageData<>();

        Page<WjAppealComment> page = PageFactory.createPage(pageQuery);

        QueryWrapper<WjAppealComment> wjAppealCommentQueryWrapper = new QueryWrapper<>();
        wjAppealCommentQueryWrapper.orderByDesc("create_time")
                .eq("user_id", CommonUtil.getNowUserId());

        page = wjAppealCommentService.page(page, wjAppealCommentQueryWrapper);


        BeanUtils.copyProperties(page, objectPageData);

        return WrapMapper.ok(objectPageData);

    }






}

