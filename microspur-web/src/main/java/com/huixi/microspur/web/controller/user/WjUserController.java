package com.huixi.microspur.web.controller.user;


import cn.hutool.core.bean.BeanUtil;
import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.constant.Constant;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.pojo.dto.user.UserProfileDTO;
import com.huixi.microspur.web.pojo.entity.user.WjUser;
import com.huixi.microspur.web.pojo.vo.user.UserDataVO;
import com.huixi.microspur.web.service.WjUserService;
import com.huixi.microspur.web.util.AlibabaOSS;
import com.huixi.microspur.web.util.CommonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * <p>
 * 用户信息表 前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Slf4j
@RestController
@RequestMapping("/wjUser")
@Api(tags = "用户模块的接口")
public class WjUserController extends BaseController {


    @Resource
    private WjUserService wjUserService;


    @Resource
    private AlibabaOSS alibabaOSS;


    @PostMapping("/uploadMaterial")
    @ApiOperation(value = "上传素材，图片哦")
    public Wrapper uploadMaterialController(MultipartFile file) throws Exception{


        String s = alibabaOSS.uploadPhotoOss(file, Constant.APPEAL_MATERIAL);


        return WrapMapper.ok(s);
    }



    /**
     * 用户修改资料
     * @Author 叶秋
     * @Date 2020/6/27 1:26
     * @param userProfileDTO
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PutMapping("/user")
    @ApiOperation(value = "查询用户信息 包括需要查询其他表的一些数据（点赞、动态...）")
    public Wrapper updateUserInfo(@RequestBody UserProfileDTO userProfileDTO){


        WjUser wjUser = new WjUser().setUserId(CommonUtil.getNowUserId());

        BeanUtil.copyProperties(userProfileDTO, wjUser);

        boolean b = wjUserService.updateById(wjUser);

        if(b){
            wjUser = wjUserService.getById(wjUser.getUserId());
        }

        return WrapMapper.ok(wjUser);

    }


    /**
     * 查询用户信息 包括需要查询其他表的一些数据（点赞、动态...）
     * @Author 叶秋
     * @Date 2020/6/1 21:55
     * @param
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @GetMapping("/userData")
    @ApiOperation(value = "查询用户信息 包括需要查询其他表的一些数据（点赞、动态...）")
    public Wrapper getByIdUserData(){

        String nowUserId = CommonUtil.getNowUserId();

        UserDataVO byIdUserData = wjUserService.getByIdUserData(nowUserId);

        return WrapMapper.ok(byIdUserData);

    }








}

