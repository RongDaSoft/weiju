package com.huixi.microspur.web.pojo.vo.dynamic;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.huixi.microspur.web.pojo.entity.dynamic.WjDynamic;
import com.huixi.microspur.web.pojo.entity.user.WjUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 *  分页查询动态 传给前端的值
 * @Author 叶秋
 * @Date 2020/4/19 21:49
 * @param
 * @return
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="分页查询动态 传给前端的值")
public class QueryDynamicVO {


    @ApiModelProperty(value = "动态id")
    private String dynamicId;

    @ApiModelProperty(value = "关联的用户id")
    private String userId;

    @ApiModelProperty(value = "动态的内容（没有标题之类的）")
    private String content;

    @ApiModelProperty(value = "动态所发素材的url地址，多个用逗号隔开")
    private String url;

    @ApiModelProperty(value = "是否点赞")
    private Boolean isEndorse;

    @ApiModelProperty(value = "动态 点赞数||赞同数")
    private Integer endorseCount;

    @ApiModelProperty(value = "动态 评论数")
    private Integer commentCount;

    @ApiModelProperty(value = "动态 浏览量")
    private Integer browseCount;

    @ApiModelProperty(value = "创建时间")
    public LocalDateTime createTime;

    @ApiModelProperty(value = "昵称")
    @TableField("nick_name")
    private String nickName;

    @ApiModelProperty(value = "头像对应的URL地址")
    @TableField("head_portrait")
    private String headPortrait;


}
