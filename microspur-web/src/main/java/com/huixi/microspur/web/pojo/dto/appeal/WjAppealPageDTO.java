package com.huixi.microspur.web.pojo.dto.appeal;

import com.huixi.microspur.commons.page.PageQuery;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppeal;
import com.huixi.microspur.web.pojo.entity.user.WjUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @ClassName WjAppealPageVO
 * @Description TODO
 * @Author Administrator
 * @Date 2020/4/21 15:33
 * @Version
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WjAppealPageDTO {

    @ApiModelProperty(value = "用户id", hidden = true)
    private String userId;

    @ApiModelProperty(value = "是否根据创建时间排序, 是否根据这个排序")
    private Boolean createTime;

    @ApiModelProperty(value = "是否根据点赞的次数排序, 是否根据这个排序")
    private Boolean endorseCount;


    @ApiModelProperty(value = "分页参数")
    private PageQuery pageQuery;

}
