package com.huixi.microspur.web.controller.dynamic;


import com.huixi.microspur.commons.base.BaseController;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 动态评论表 前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Slf4j
@RestController
@RequestMapping("/wjDynamicComment")
@Api(tags = "动态评论模块")
public class WjDynamicCommentController extends BaseController {

}

