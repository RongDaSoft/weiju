package com.huixi.microspur.web.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealAddress;
import com.huixi.microspur.web.mapper.WjAppealAddressMapper;
import com.huixi.microspur.web.service.WjAppealAddressService;
import org.springframework.stereotype.Service;

/**
 *  诉求地址 服务实现类
 * @Author 叶秋 
 * @Date 2020/3/25 2:23
 * @param 
 * @return 
 **/
@Service
public class WjAppealAddressServiceImpl extends ServiceImpl<WjAppealAddressMapper, WjAppealAddress> implements WjAppealAddressService {
}
