package com.huixi.microspur.web.pojo.vo.chat;

import com.huixi.microspur.commons.page.PageQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 *  查询聊天记录所用到的 条件
 * @Author 叶秋
 * @Date 2020/5/3 12:44
 * @param
 * @return
 **/
@Data
public class WjChatRecordPageVO {


    @NotNull(message = "聊天室id为空")
    @ApiModelProperty(value = "聊天室id")
    private String chatId;

    @ApiModelProperty("分页参数")
    private PageQuery pageQuery;

}
