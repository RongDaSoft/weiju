package com.huixi.microspur.web.pojo.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.microspur.commons.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户足迹表
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_user_track")
@ApiModel(value="WjUserTrack对象", description="用户足迹表")
public class WjUserTrack extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "user_track_id", type = IdType.AUTO)
    private Integer userTrackId;

    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private String userId;

    @ApiModelProperty(value = "用户name")
    @TableField("user_name")
    private String userName;

    @ApiModelProperty(value = "ip")
    @TableField("ip")
    private String ip;

    @ApiModelProperty(value = "类型(查询，删除，修改......)")
    @TableField("type")
    private String type;

    @ApiModelProperty(value = "描述")
    @TableField("text")
    private String text;

    @ApiModelProperty(value = "参数")
    @TableField("param")
    private String param;

    @TableField("create_by")
    private String createBy;

    @TableField("update_by")
    private String updateBy;

    @ApiModelProperty(value = "标识(0:无,1:用户,2:诉求,3:动态,4:语录)")
    @TableField("t_flag")
    private Integer tFlag;

    @ApiModelProperty(value = "目标id")
    @TableField("target_id")
    private String targetId;

    @ApiModelProperty(value = "设备品牌")
    @TableField("brand")
    private String brand;

    @ApiModelProperty(value = "设备型号")
    @TableField("model")
    private String model;

    @ApiModelProperty(value = "微信版本号")
    @TableField("version")
    private String version;

    @ApiModelProperty(value = "操作系统及版本")
    @TableField("system")
    private String system;

    @ApiModelProperty(value = "客户端平台")
    @TableField("platform")
    private String platform;


}
