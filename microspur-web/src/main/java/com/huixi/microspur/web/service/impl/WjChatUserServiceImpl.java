package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.pojo.entity.chat.WjChatUser;
import com.huixi.microspur.web.mapper.WjChatUserMapper;
import com.huixi.microspur.web.service.WjChatUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 聊天室对应的用户 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjChatUserServiceImpl extends ServiceImpl<WjChatUserMapper, WjChatUser> implements WjChatUserService {

}
