package com.huixi.microspur.web.controller.chat;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.pojo.entity.chat.WjChatUser;
import com.huixi.microspur.web.service.WjChatUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * 聊天室对应的用户 前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Slf4j
@RestController
@RequestMapping("/wjChatUser")
@Api(tags = "聊天室-对应的用户")
public class WjChatUserController extends BaseController {

    @Resource
    private WjChatUserService wjChatUserService;


    /**
     * 根据聊天室id 查询聊天室 对应的用户
     * @Author 叶秋
     * @Date 2020/4/24 14:41
     * @param chatId 聊天室id
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @ApiOperation(value = "根据聊天室id 查询聊天室 对应的用户")
    @GetMapping("/queryChatUser/{chatId}")
    public Wrapper queryChatUser(@Valid @NotNull(message = "聊天室id为空") @PathVariable String chatId){

        QueryWrapper<WjChatUser> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("chat_id", chatId);

        List<WjChatUser> list = wjChatUserService.list(objectQueryWrapper);

        return WrapMapper.ok(list);

    }





}

