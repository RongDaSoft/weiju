package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.web.pojo.vo.dynamic.WjDynamicPageVO;
import com.huixi.microspur.web.pojo.entity.dynamic.WjDynamic;

/**
 * <p>
 * 动态表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjDynamicService extends IService<WjDynamic> {

    /**
     *  分页查询动态
     * @Author 叶秋
     * @Date 2020/4/22 11:38
     * @param wjDynamicPageVO
     * @return java.util.List<com.huixi.microspur.web.pojo.vo.dynamic.QueryDynamicVO>
     **/
    PageData listPageDynamic(WjDynamicPageVO wjDynamicPageVO);

}
