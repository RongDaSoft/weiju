package com.huixi.microspur.web.controller.appeal;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.enums.AppealTagEnum;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealTag;
import com.huixi.microspur.web.service.WjAppealTagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * 诉求-对应标签 前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@RestController
@RequestMapping("/wjAppealTag")
@Api(tags = "诉求-标签的接口")
public class WjAppealTagController extends BaseController {

    @Resource
    private WjAppealTagService wjAppealTagService;



    /**
     *  根据id 查询其标签
     * @Author 叶秋
     * @Date 2020/4/13 22:56
     * @param
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @GetMapping("/appealTag/{appealId}")
    @ApiOperation(value = "根据id 查询其标签")
    public Wrapper queryWjAppealTagById(@Valid @NotNull(message = "诉求id为空") @PathVariable String appealId){

        QueryWrapper<WjAppealTag> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("appeal_id", appealId);

        List<WjAppealTag> list = wjAppealTagService.list(objectQueryWrapper);

        return WrapMapper.ok(list);

    }



    /**
     *  查询出所有的诉求 标签
     * @Author 叶秋
     * @Date 2020/7/13 21:43
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @GetMapping("/appealTag")
    @ApiOperation(value = "查询出所有的诉求 标签")
    public Wrapper queryAllWjAppealTag(){

        AppealTagEnum[] values = AppealTagEnum.values();

        return WrapMapper.ok(values);

    }




}

