package com.huixi.microspur.web.pojo.vo.dynamic;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.huixi.microspur.commons.page.PageQuery;
import com.huixi.microspur.web.pojo.entity.dynamic.WjDynamic;
import com.huixi.microspur.web.pojo.entity.user.WjUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 *  动态 分页的VO类
 * @Author 叶秋 
 * @Date 2020/4/19 21:14
 * @param 
 * @return 
 **/
@Data
public class WjDynamicPageVO {

    @ApiModelProperty(value = "创建时间")
    public Boolean createTime;

    @ApiModelProperty(value = "动态 点赞数||赞同数")
    private Boolean endorseCount;

    @ApiModelProperty(value = "用户id", hidden = true)
    private String userId;

    @ApiModelProperty("分页参数")
    private PageQuery pageQuery;



}
