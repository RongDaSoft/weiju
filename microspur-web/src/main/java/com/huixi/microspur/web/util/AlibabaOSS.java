package com.huixi.microspur.web.util;

import cn.hutool.core.util.IdUtil;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;
import com.aliyun.oss.model.PutObjectResult;
import com.huixi.microspur.commons.constant.Constant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * 阿里巴巴OSS存储库,对上传下载删除修改的一个封装
 * 注意：用的时候必须使用@AutoWrite ，不然不会生效
 *
 * @author: 李辉
 * @date: 2019/9/8 1:01
 * @param:
 * @return:
 */
@Configuration
@Slf4j
public class AlibabaOSS {


    /**
      * 外网访问
      *@author: 李辉
      *@date: 2019/10/27 17:02
      */
    @Value("${alibabaOSS.endpoint}")
    private String endpoint;

    /**
      * 内网访问
      *@author: 李辉
      *@date: 2019/10/27 17:03
      */
    @Value("${alibabaOSS.endpointIntranet}")
    private String endpointIntranet;

    @Value("${alibabaOSS.accessKeyId}")
    private String accessKeyId;

    @Value("${alibabaOSS.accessKeySecret}")
    private String accessKeySecret;

    @Value("${alibabaOSS.bucketName}")
    private String bucketName;

    /**
      * 操作外网
      *@author: 李辉
      *@date: 2019/10/27 16:34
      *@param:
      *@return:
      */
    private OSS getOssClient() {

        // 创建OSSClient实例。官方封装了对操作文件的方法
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        return ossClient;

    }

    /**
      * 操作内网
      *@author: 李辉
      *@date: 2019/10/27 16:34
      *@param:
      *@return:
      */
     private OSS getOssIntranetClient() {

        // 创建OSSClient实例。官方封装了对操作文件的方法
        OSS ossClient = new OSSClientBuilder().build(endpointIntranet, accessKeyId, accessKeySecret);
        return ossClient;

    }


    /**
     * @功能:上传图片文件
     * @param file 文件
     * @param uploadFolder 上传的文件夹
     * @return
     * @throws Exception
     * @作者: 恒
     * @创建时间: 2017年8月10日下午5:26:17
     */
    public String uploadPhotoOss(MultipartFile file, String uploadFolder) throws Exception {

        String originalFilename = file.getOriginalFilename();
        // 截取后的效果 .jpg
        String fileType = originalFilename.substring(originalFilename.lastIndexOf(".")).toLowerCase();

        // 新的文件名
        String uploadObject = IdUtil.simpleUUID() + fileType;

        // 上传的文件夹
        String uploadFolderObject = uploadFolder + "/" + uploadObject;

        try {
            InputStream inputStream = file.getInputStream();
            OSS ossClient = getOssClient();
            ossClient.putObject(bucketName, uploadFolderObject, inputStream);
            return Constant.ALIOSSDOMAINNAME+uploadFolderObject;
        } catch (Exception e) {
            throw new Exception("图片上传失败");
        }


    }


}
