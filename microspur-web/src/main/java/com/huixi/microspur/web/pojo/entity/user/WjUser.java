package com.huixi.microspur.web.pojo.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.microspur.commons.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_user")
@ApiModel(value="WjUser对象", description="用户信息表")
public class WjUser extends BaseEntity implements UserDetails {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户id")
    @TableId( value = "user_id", type = IdType.INPUT)
    private String userId;

    @ApiModelProperty(value = "昵称")
    @TableField("nick_name")
    private String nickName;

    @ApiModelProperty(value = "真实姓名")
    @TableField("real_name")
    private String realName;

    @ApiModelProperty(value = "头像对应的URL地址")
    @TableField("head_portrait")
    private String headPortrait;

    @ApiModelProperty(value = "性别 改用String，活泼一点。自定义都可以")
    @TableField("sex")
    private String sex;

    @ApiModelProperty(value = "手机号")
    @TableField("mobile")
    private String mobile;

    @ApiModelProperty(value = "公司")
    @TableField("company")
    private String company;

    @ApiModelProperty(value = "电子邮箱")
    @TableField("email")
    private String email;

    @ApiModelProperty(value = "个性签名（冗余）")
    @TableField("signature")
    private String signature;

    @ApiModelProperty(value = "简介")
    @TableField("introduce")
    private String introduce;

    @ApiModelProperty(value = "地址")
    @TableField("address")
    private String address;

    @ApiModelProperty(value = "密码（冗余）")
    @TableField("password")
    private String password;

    @ApiModelProperty(value = "身份证号")
    @TableField("identity_card")
    private String identityCard;

    @ApiModelProperty(value = "创建人")
    @TableField("create_by")
    private String createBy;

    @ApiModelProperty(value = "修改人")
    @TableField("update_by")
    private String updateBy;







    /**
     * 用户角色
     */
    @TableField(exist = false)
    private Collection<GrantedAuthority> authorities;
    /**
     * 账户是否过期
     */
    @TableField(exist = false)
    private boolean isAccountNonExpired = false;
    /**
     * 账户是否被锁定
     */
    @TableField(exist = false)
    private boolean isAccountNonLocked = false;
    /**
     * 证书是否过期
     */
    @TableField(exist = false)
    private boolean isCredentialsNonExpired = false;
    /**
     * 账户是否有效
     */
    @TableField(exist = false)
    private boolean isEnabled = true;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
