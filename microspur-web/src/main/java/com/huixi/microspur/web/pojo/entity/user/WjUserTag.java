package com.huixi.microspur.web.pojo.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.huixi.microspur.commons.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 叶秋
 * @since 2020-07-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_user_tag")
@ApiModel(value="WjUserTag对象", description="")
public class WjUserTag extends BaseEntity implements Serializable  {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户标签表主键")
    @TableId(value = "user_tag_id", type = IdType.ASSIGN_UUID)
    private String userTagId;

    @ApiModelProperty(value = "用户id")
    @TableId("user_id")
    private String userId;

    @ApiModelProperty(value = "标签id")
    @TableField("tag_id")
    private Integer tagId;

    @ApiModelProperty(value = "标签名字")
    @TableField("tag_value")
    private String tagValue;

    @ApiModelProperty(value = "是否选中")
    @TableField("check")
    private Boolean check;


    @ApiModelProperty(value = "创建人")
    @TableField("create_by")
    private String createBy;


    @ApiModelProperty(value = "修改人")
    @TableField("update_by")
    private String updateBy;



}
