package com.huixi.microspur.web.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.commons.page.PageFactory;
import com.huixi.microspur.web.pojo.entity.chat.WjChat;
import com.huixi.microspur.web.pojo.entity.chat.WjChatUser;
import com.huixi.microspur.web.pojo.vo.appeal.QueryAppealVO;
import com.huixi.microspur.web.pojo.dto.appeal.WjAppealPageDTO;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppeal;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealMaterial;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealTag;
import com.huixi.microspur.web.pojo.entity.user.WjUser;
import com.huixi.microspur.web.mapper.WjAppealMapper;
import com.huixi.microspur.web.service.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 诉求表 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjAppealServiceImpl extends ServiceImpl<WjAppealMapper, WjAppeal> implements WjAppealService {

    @Resource
    private WjAppealEndorseService wjAppealEndorseService;

    @Resource
    private WjUserService wjUserService;

    @Resource
    private WjAppealMaterialService wjAppealMaterialService;

    @Resource
    private WjAppealTagService wjAppealTagService;

    @Resource
    private WjChatService wjChatService;

    @Resource
    private WjChatUserService wjChatUserService;


    /**
     * 按条件分页查询 诉求
     *
     * @param wjAppealPageDTO
     * @return
     * @Author 叶秋
     * @Date 2020/2/26 21:14
     **/
    @Override
    public PageData listPageAppeal(WjAppealPageDTO wjAppealPageDTO) {

        // 最终传过去的值
        PageData pageData = new PageData();

        List<QueryAppealVO> queryAppealVOList = new ArrayList<>();

        // 分页参数
        Page<WjAppeal> page = PageFactory.createPage(wjAppealPageDTO.getPageQuery());
        // 查询的条件
        QueryWrapper<WjAppeal> objectQueryWrapper = new QueryWrapper<>();


        // 按照创建时间来查询
        if (wjAppealPageDTO.getCreateTime()) {
            objectQueryWrapper.orderByDesc("create_time");
        }
        // 点赞量
        if (wjAppealPageDTO.getEndorseCount()) {
            objectQueryWrapper.orderByDesc("endorse_count");
        }
        //

        // 查询到的参数
        Page<WjAppeal> page1 = page(page, objectQueryWrapper);
        List<WjAppeal> records = page1.getRecords();

        QueryAppealVO queryAppealVO;
        // 诉求素材表查询条件
        QueryWrapper<WjAppealMaterial> wjAppealMaterialQueryWrapper;
        // 诉求标签表查询条件
        QueryWrapper<WjAppealTag> wjAppealTagQueryWrapper;

        for (WjAppeal record : records) {
            queryAppealVO = new QueryAppealVO();

            // 查询是否点赞
            if (wjAppealPageDTO.getUserId() != null) {
                Boolean endorse = wjAppealEndorseService.isEndorse(record.getAppealId(), wjAppealPageDTO.getUserId());
                queryAppealVO.setIsEndorse(endorse);
            } else {
                queryAppealVO.setIsEndorse(false);
            }

            // 查询点赞总数量
            int totalCount = wjAppealEndorseService.getTotalCount(record.getAppealId());
            record.setEndorseCount(totalCount);


            // 查询发帖人信息
            WjUser wjUser = wjUserService.getById(record.getUserId());
            queryAppealVO.setNickName(wjUser.getNickName())
                    .setHeadPortrait(wjUser.getHeadPortrait());

            // 查询帖子相关素材
            wjAppealMaterialQueryWrapper = new QueryWrapper<>();
            wjAppealMaterialQueryWrapper.eq("appeal_id", record.getAppealId());
            List<WjAppealMaterial> list = wjAppealMaterialService.list(wjAppealMaterialQueryWrapper);
            queryAppealVO.setAppealMaterial(list);

            // 查询帖子相关的 标签
            wjAppealTagQueryWrapper = new QueryWrapper<>();
            wjAppealTagQueryWrapper.eq("appeal_id", record.getAppealId());
            List<WjAppealTag> list1 = wjAppealTagService.list(wjAppealTagQueryWrapper);
            queryAppealVO.setAppealTag(list1);

            // 赋值给 诉求类
            BeanUtil.copyProperties(record, queryAppealVO);


            // end
            queryAppealVOList.add(queryAppealVO);
        }


        BeanUtil.copyProperties(page1, pageData);
        pageData.setRecords(queryAppealVOList);

        return pageData;
    }


    /**
     * 根据所给的用户id 分页查询他所有的诉求（就是查询我的 诉求）
     * @Author 叶秋
     * @Date 2020/6/3 22:04
     * @param wjAppealPageVO
     * @return com.huixi.microspur.commons.page.PageData
     **/
    @Override
    public PageData ListByUserIdMyAppeal(WjAppealPageDTO wjAppealPageVO) {

        // 最终传过去的值
        PageData pageData = new PageData();

        List<QueryAppealVO> queryAppealVOList = new ArrayList<>();

        // 分页参数
        Page<WjAppeal> page = PageFactory.createPage(wjAppealPageVO.getPageQuery());
        // 查询的条件
        QueryWrapper<WjAppeal> objectQueryWrapper = new QueryWrapper<>();


        // 按照创建时间来查询
        objectQueryWrapper.orderByDesc("create_time");

        // 查询自己的
        objectQueryWrapper.eq("user_id", wjAppealPageVO.getUserId());

        // 查询到的参数
        Page<WjAppeal> page1 = page(page, objectQueryWrapper);
        List<WjAppeal> records = page1.getRecords();

        QueryAppealVO queryAppealVO;
        // 诉求素材表查询条件
        QueryWrapper<WjAppealMaterial> wjAppealMaterialQueryWrapper;
        // 诉求标签表查询条件
        QueryWrapper<WjAppealTag> wjAppealTagQueryWrapper;

        for (WjAppeal record : records) {
            queryAppealVO = new QueryAppealVO();

            // 查询是否点赞
            if (wjAppealPageVO.getUserId() != null) {
                Boolean endorse = wjAppealEndorseService.isEndorse(record.getAppealId(),
                        wjAppealPageVO.getUserId());
                queryAppealVO.setIsEndorse(endorse);
            } else {
                queryAppealVO.setIsEndorse(false);
            }

            // 查询点赞总数量
            int totalCount = wjAppealEndorseService.getTotalCount(record.getAppealId());
            record.setEndorseCount(totalCount);


            // 查询发帖人信息
            WjUser wjUser = wjUserService.getById(record.getUserId());
            queryAppealVO.setNickName(wjUser.getNickName())
                    .setHeadPortrait(wjUser.getHeadPortrait());

            // 查询帖子相关素材
            wjAppealMaterialQueryWrapper = new QueryWrapper<>();
            wjAppealMaterialQueryWrapper.eq("appeal_id", record.getAppealId());
            List<WjAppealMaterial> list = wjAppealMaterialService.list(wjAppealMaterialQueryWrapper);
            queryAppealVO.setAppealMaterial(list);

            // 查询帖子相关的 标签
            wjAppealTagQueryWrapper = new QueryWrapper<>();
            wjAppealTagQueryWrapper.eq("appeal_id", record.getAppealId());
            List<WjAppealTag> list1 = wjAppealTagService.list(wjAppealTagQueryWrapper);
            queryAppealVO.setAppealTag(list1);

            // 赋值给 诉求类
            BeanUtil.copyProperties(record, queryAppealVO);


            // end
            queryAppealVOList.add(queryAppealVO);

        }


        BeanUtil.copyProperties(page1, pageData);
        pageData.setRecords(queryAppealVOList);

        return pageData;

    }


    /**
     *  判断是否是自己发布的
     * @Author 叶秋
     * @Date 2020/7/1 21:06
     * @param appealId 诉求id
     * @param userId 用户id
     * @return java.lang.Boolean
     **/
    @Override
    public Boolean judgeAppealIsMe(String appealId, String userId) {

        QueryWrapper<WjAppeal> wjAppealQueryWrapper = new QueryWrapper<>();
        wjAppealQueryWrapper.eq("appeal_id", appealId).eq("user_id", userId);

        WjAppeal one = getOne(wjAppealQueryWrapper);

        if(ObjectUtil.isNotNull(one)){
            return true;
        }


        return false;
    }


    /**
     *  校验于此诉求沟通的人是否已满
     * @Author 叶秋
     * @Date 2020/7/9 21:44
     * @param appealId 诉求id
     * @return java.lang.Boolean
     **/
    @Override
    public Boolean verifyAppealFull(String appealId) {

        QueryWrapper<WjChat> wjChatQueryWrapper = new QueryWrapper<>();
        wjChatQueryWrapper.eq("appeal_id", appealId);

        WjChat one = wjChatService.getOne(wjChatQueryWrapper);

        if(ObjectUtil.isNotNull(one)){
            return true;
        }


        return false;
    }







}
