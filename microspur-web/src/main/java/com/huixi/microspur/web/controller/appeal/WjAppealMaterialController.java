package com.huixi.microspur.web.controller.appeal;


import cn.hutool.core.util.StrUtil;
import com.aliyun.oss.OSS;
import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.constant.Constant;
import com.huixi.microspur.commons.enums.ErrorCodeEnum;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealMaterial;
import com.huixi.microspur.web.service.WjAppealMaterialService;
import com.huixi.microspur.web.util.AlibabaOSS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.List;

/**
 * <p>
 * 诉求素材表-存储素材涉及的图片，或者大文件 前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@RestController
@RequestMapping("/wjAppealMaterial")
@Api(tags = "诉求-素材接口(诉求模块的附属接口)")
public class WjAppealMaterialController extends BaseController {

    @Resource
    private WjAppealMaterialService wjAppealMaterialService;

    @Resource
    private AlibabaOSS alibabaOSS;


    @PostMapping("/uploadMaterial")
    @ApiOperation(value = "上传素材，图片哦")
    public Wrapper uploadMaterialController(MultipartFile file) throws Exception{


        String s = alibabaOSS.uploadPhotoOss(file, Constant.APPEAL_MATERIAL);


        return WrapMapper.ok(s);
    }


    @GetMapping("/listByAppealId/{appealId}")
    @ApiOperation(value = "根据诉求id查询所有相关的诉求素材(图片,音频,视频)")
    public Wrapper listByAppealId(@PathVariable String appealId){

        if(StrUtil.isEmpty(appealId)){
            return WrapMapper.error(ErrorCodeEnum.APPEALMATERIAL001.msg());
        }

        List<WjAppealMaterial> wjAppealMaterials = wjAppealMaterialService.listByAppealId(appealId);

        return WrapMapper.ok(wjAppealMaterials);

    }

}

