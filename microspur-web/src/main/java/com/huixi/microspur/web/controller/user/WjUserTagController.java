package com.huixi.microspur.web.controller.user;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.huixi.microspur.commons.enums.UserTagEnum;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.pojo.entity.user.WjUserTag;
import com.huixi.microspur.web.service.WjUserTagService;
import com.huixi.microspur.web.util.CommonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import com.huixi.microspur.commons.base.BaseController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 叶秋
 * @since 2020-07-03
 */
@RestController
@RequestMapping("/wjUserTag")
@Api(tags = "用户标签表")
public class WjUserTagController extends BaseController {


    @Resource
    private WjUserTagService wjUserTagService;


    /**
     * 设置用户显示的标签
     * @Author 叶秋
     * @Date 2020/7/6 22:23
     * @param tagId 标签id
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/setUpTag/{tagId}")
    @ApiOperation(value = "设置用户显示的标签")
    public Wrapper setUpTag(@Valid @NotNull(message = "标签id不能为空") @PathVariable Integer tagId){

        String nowUserId = CommonUtil.getNowUserId();

        Boolean aBoolean = wjUserTagService.detectionIsPossess(nowUserId, tagId);
        Boolean easterEgg = UserTagEnum.USER_TAG_2333.id().equals(tagId);
        // 如果不是彩蛋 以及 不是自己所拥有的
        if(!easterEgg && !aBoolean){
            return WrapMapper.error("你没有此标签");
        }


        Boolean aBoolean1 = wjUserTagService.settingTag(nowUserId, tagId);

        return WrapMapper.ok(aBoolean1);
    }



    /**
     * 查询我的所拥有的标签
     * @Author 叶秋
     * @Date 2020/7/7 21:33
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @GetMapping("/queryMyTag")
    @ApiOperation(value = "查询我的所拥有的标签")
    public Wrapper queryMyTag(){

        String nowUserId = CommonUtil.getNowUserId();

        QueryWrapper<WjUserTag> wjUserTagQueryWrapper = new QueryWrapper<>();
        wjUserTagQueryWrapper.eq("user_id", nowUserId);

        List<WjUserTag> list = wjUserTagService.list(wjUserTagQueryWrapper);


        return WrapMapper.ok(list);

    }
    
    
    /**
     *  查询 所有的标签
     * @Author 叶秋 
     * @Date 2020/7/9 21:13
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @GetMapping("/queryAllUserTag")
    @ApiOperation(value = "查询 所有的标签")
    public Wrapper queryAllUserTag(){

        UserTagEnum[] values = UserTagEnum.values();

        return WrapMapper.ok(values);
        
    }





}

